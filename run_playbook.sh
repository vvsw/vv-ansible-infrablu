#!/bin/bash
ansible-playbook -i inventory.yml -i cgu.box.inventory.yml --ask-become-pass playbook.yml \
-e @roles/vv_role_lxd/defaults/secrets.sec \
--vault-password-file .ansible_vault_key $1
