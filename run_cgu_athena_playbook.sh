#!/bin/bash
ansible-playbook -i inventory.yml -i cgu.box.inventory.yml --ask-become-pass cgu-athena-playbook.yml \
--vault-password-file .ansible_vault_key $1
